import asyncio
import websockets


# この関数に通信しているときに行う処理を書く。
# クライアントが接続している間は下の関数が常に回っている
async def handler(websocket):
    # クライアントからのメッセージを取り出してそのまま送り返す（Echo）
    async for message in websocket:
        print(message)
        await websocket.send(f"あなたが送信した文字は「{message}」です")


async def main():
    async with websockets.serve(handler, "0.0.0.0", 15140):
        await asyncio.Future()  # run forever

asyncio.run(main())
