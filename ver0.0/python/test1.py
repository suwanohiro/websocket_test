import websockets


def on_message(wsapp, message):
    print(message)


wsapp = websockets.WebSocketApp(
    "wss://suwanohiro.f5.si:18000", on_message=on_message)
wsapp.run_forever()
