import threading
import PySimpleGUI as sg
from websocket_server import WebsocketServer

# 自作ライブラリ
from Library import Command as cmd


class Function(threading.Thread):
    import asyncio
    import websockets

    Local = True
    Power = False
    PortNumber = 15140
    server = object
    window = object
    Global_IP_Address = ""

    def init(window, Global_IP_Address):
        Function.window = window
        Function.Global_IP_Address = Global_IP_Address

    def run(self):
        if Function.Local:
            Host = "127.0.0.1"
        else:
            Host = "0.0.0.0"

        Function.server = WebsocketServer(
            port=int(Function.PortNumber), host=Host)
        Function.server.set_fn_new_client(Function.new_client)
        Function.server.set_fn_message_received(Function.send_msg_allclient)
        try:
            Function.server.run_forever()
        except:
            print("終了します")  # ※表示されない

    def new_client(client, server):
        server.send_message_to_all("New client has joined")

    def send_msg_allclient(client, server, message):
        # データを受信した時に実行される処理

        class datas:
            client = object
            server = object
            message = object

        datas.client = client
        datas.server = server
        datas.message = message

        cmd.Read(datas)

        print(message)
        server.send_message_to_all(message)

    def BootUp(Port):
        try:
            if Port < 1 or Port > 65535:
                raise
            Function.Power = True
            Function.PortNumber = int(Port)
            Sub = Function()
            Sub.start()
            print(f"{Port}番ポートでサーバーを起動しました。")
            Function.window["ServerStatus"].update(
                background_color="light green")
            Function.window["ServerPort"].update(str(Port))
            Function.window["ServerAccessAddress"].update(
                f"{Function.Global_IP_Address}:{Port}")
        except:
            Function.Power = False
            sg.popup("サーバーの起動に失敗しました。", "エラー")
            Function.window["ServerPower"].update("サーバー起動")

    def PortChange():
        InputText = sg.popup_get_text("ポート番号を入力してください", title="ポート番号変更")
        try:
            InputNumber = int(InputText)

            if InputNumber < 1 or InputNumber > 65535:
                raise

            Function.PortNumber = InputNumber
            string = f"{Function.Global_IP_Address}:{Function.PortNumber}"

            Function.window["ServerPort"].update(InputNumber)
            Function.window["ServerAccessAddress"].update(string)

            print(InputNumber)
        except:
            if InputText != None:
                if InputText.isdigit():
                    sg.popup("1～65535の数値を入力してください", "エラー")
                else:
                    sg.popup("数値を入力してください", "エラー")
