import PySimpleGUI as sg
import requests

# 自作ライブラリ
from Library import Server


Global_IP_Address = requests.get("https://api.ipify.org/").text

# 画面構成
layout = [
    [sg.Text("サーバー"), sg.Text(
        f" ({Global_IP_Address})", key="Global_IP")],
    [sg.HorizontalSeparator()],
    [sg.Text("稼働状態　："), sg.Text(
        "　　　", key="ServerStatus", background_color="red")],
    [sg.Text("使用ポート："), sg.Text(Server.Function.PortNumber, key="ServerPort")],
    [sg.Text("アドレス　："), sg.Text(
        f"{Global_IP_Address}:{Server.Function.PortNumber}", key="ServerAccessAddress")],
    [sg.HorizontalSeparator()],
    [sg.Button("サーバー起動", key="ServerPower", size=(15, 1)),
     sg.Button("ポート番号変更", key="ChangePortNumber", size=(15, 1))]
]

# ウィンドウの生成
window = sg.Window("サーバー管理フォーム", layout, size=(300, 170))

# サーバークラスにフォーム情報等を渡す
Server.Function.init(window=window, Global_IP_Address=Global_IP_Address)

# イベントループ
while True:
    event, values = window.read()

    if event == sg.WIN_CLOSED:
        # ウィンドウが閉じられた時に処理を抜ける

        if Server.Function.Power == True:
            Server.Function.Server.Function.shutdown_gracefully()

        break
    elif event == "ServerPower":
        if Server.Function.Power == False:
            window["ServerPower"].update("サーバー停止")
            Server.Function.BootUp(Server.Function.PortNumber)
        else:
            window["ServerStatus"].update(background_color="red")
            Server.Function.Power = False
            Server.Function.server.shutdown_gracefully()
            window["ServerPower"].update("サーバー起動")
    elif event == "ChangePortNumber":
        if Server.Function.Power == False:
            Server.Function.PortChange()
        else:
            sg.popup("サーバーを停止してください", title="エラー")

window.close()
