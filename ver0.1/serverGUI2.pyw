import PySimpleGUI as sg
import requests
import threading
import sys
import time
from websocket_server import WebsocketServer

# Serverクラスを並列処理させること！


class Server(threading.Thread):
    import asyncio
    import websockets

    Local = False
    Power = False
    PortNumber = 15140
    server = object

    def run(self):
        if Server.Local:
            Host = "127.0.0.1"
        else:
            Host = "0.0.0.0"

        Server.server = WebsocketServer(
            port=int(Server.PortNumber), host=Host)
        Server.server.set_fn_new_client(Server.new_client)
        Server.server.set_fn_message_received(Server.send_msg_allclient)
        try:
            Server.server.run_forever()
        except:
            print("終了します")  # ※表示されない

    def new_client(client, server):
        server.send_message_to_all("New client has joined")

    def send_msg_allclient(client, server, message):
        # データを受信した時に実行される処理
        print(message)
        server.send_message_to_all(message)

    def BootUp(Port):
        try:
            if Port < 1 or Port > 65535:
                raise
            Server.Power = True
            Server.PortNumber = int(Port)
            Sub = Server()
            Sub.start()
            print(f"{Port}番ポートでサーバーを起動しました。")
            window["ServerStatus"].update(background_color="light green")
            window["ServerPort"].update(str(Port))
            window["ServerAccessAddress"].update(f"{Global_IP_Address}:{Port}")
        except:
            Server.Power = False
            sg.popup("サーバーの起動に失敗しました。", "エラー")
            window["ServerPower"].update("サーバー起動")

    def PortChange():
        InputText = sg.popup_get_text("ポート番号を入力してください", title="ポート番号変更")
        try:
            InputNumber = int(InputText)

            if InputNumber < 1 or InputNumber > 65535:
                raise

            Server.PortNumber = InputNumber
            string = f"{Global_IP_Address}:{Server.PortNumber}"

            window["ServerPort"].update(InputNumber)
            window["ServerAccessAddress"].update(string)

            print(InputNumber)
        except:
            if InputText != None:
                if InputText.isdigit():
                    sg.popup("1～65535の数値を入力してください", "エラー")
                else:
                    sg.popup("数値を入力してください", "エラー")


Global_IP_Address = requests.get("https://api.ipify.org/").text

# 画面構成
layout = [
    [sg.Text("サーバー"), sg.Text(
        f" ({Global_IP_Address})", key="Global_IP")],
    [sg.HorizontalSeparator()],
    [sg.Text("稼働状態　："), sg.Text(
        "　　　", key="ServerStatus", background_color="red")],
    [sg.Text("使用ポート："), sg.Text(Server.PortNumber, key="ServerPort")],
    [sg.Text("アドレス　："), sg.Text(
        f"{Global_IP_Address}:{Server.PortNumber}", key="ServerAccessAddress")],
    [sg.HorizontalSeparator()],
    [sg.Button("サーバー起動", key="ServerPower", size=(15, 1)),
     sg.Button("ポート番号変更", key="ChangePortNumber", size=(15, 1))]
]

# ウィンドウの生成
window = sg.Window("サーバー管理フォーム", layout, size=(300, 170))


# イベントループ
while True:
    event, values = window.read()

    if event == sg.WIN_CLOSED:
        # ウィンドウが閉じられた時に処理を抜ける

        if Server.Power == True:
            Server.server.shutdown_gracefully()

        break
    elif event == "ServerPower":
        if Server.Power == False:
            window["ServerPower"].update("サーバー停止")
            Server.BootUp(Server.PortNumber)
        else:
            window["ServerStatus"].update(background_color="red")
            Server.Power = False
            Server.server.shutdown_gracefully()
            window["ServerPower"].update("サーバー起動")
    elif event == "ChangePortNumber":
        if Server.Power == False:
            Server.PortChange()
        else:
            sg.popup("サーバーを停止してください", title="エラー")

window.close()
