import PySimpleGUI as sg
import requests
import threading
import sys
import time

# Serverクラスを並列処理させること！


class Server(threading.Thread):
    import asyncio
    import websockets

    Power = False
    PortNumber = 0

    def run(self):
        Server.asyncio.run(Server.main())

    # この関数に通信しているときに行う処理を書く。
    # クライアントが接続している間は下の関数が常に回っている

    async def handler(websocket):
        print("a")

        # クライアントからのメッセージを取り出してそのまま送り返す（Echo）
        async for message in websocket:
            print(message)
            await websocket.send(f"あなたが送信した文字は「{message}」です")

    async def main():
        print("サーバー起動中")
        async with Server.websockets.serve(Server.handler, "0.0.0.0", int(Server.PortNumber)):
            await Server.asyncio.Future()  # run forever

    def BootUp(Port):
        try:
            if Port < 1 or Port > 65535:
                raise
            Server.Power = True
            Server.PortNumber = int(Port)
            Sub = Server()
            Sub.start()
            print(f"{Port}番ポートでサーバーを起動しました。")
            window["ServerStatus"].update(background_color="light green")
            window["ServerPort"].update(str(Port))
            window["ServerAccessAddress"].update(f"{Global_IP_Address}:{Port}")
        except:
            print("サーバーの起動に失敗しました。")
            sg.popup("サーバーの起動に失敗しました。", "エラー")


class Form(threading.Thread):

    def run(self):
        print("a")


Global_IP_Address = requests.get("https://api.ipify.org/").text

# 画面構成
layout = [
    [sg.Text("サーバー"), sg.Text(
        f" ({Global_IP_Address})", key="Global_IP")],
    [sg.HorizontalSeparator()],
    [sg.Text("稼働状態　："), sg.Text(
        "　　　", key="ServerStatus", background_color="red")],
    [sg.Text("使用ポート："), sg.Text("", key="ServerPort")],
    [sg.Text("アドレス　："), sg.Text("", key="ServerAccessAddress")],
    [sg.HorizontalSeparator()],
    [sg.Button("サーバー起動", key="ServerBootUp"),
     sg.Button("サーバー停止", key="ServerStop")]
]

# ウィンドウの生成
window = sg.Window("フォームタイトル", layout)


# イベントループ
while True:
    event, values = window.read()

    if event == sg.WIN_CLOSED:
        # ウィンドウが閉じられた時に処理を抜ける
        break
    elif event == "test":
        print("testボタンが押されました。")
    elif event == "asd":
        res = requests.get("https://api.ipify.org/").text
        print(str(res))
        print("keyがasdのボタンが押されました。")
    elif event == "ServerBootUp":
        Server.BootUp(15140)
    elif event == "ServerStop":
        Server.Power = False
        Server.asyncio.CancelledError()

window.close()
