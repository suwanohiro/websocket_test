import socketserver
from struct import unpack

"""
今回サーバ↔︎クライアント間で
やりとりするデータの構造

+-----------------------------+
| ヘッダー                            |
+-----------------------------+
| 内容: データサイズ            |
| サイズ: 4byte(固定)        |
+-----------------------------+
+-----------------------------+
| データ                               |
+-----------------------------+
| 内容: 実際のデータ           |
| サイズ: 上記指定のサイズ |
+-----------------------------+
"""
# ヘッダサイズ (固定長)
HEADER_SIZE = 4

# サーバのアドレスとポート
ADDR = "0.0.0.0"
PORT = 18000


class MyTCPHandler(socketserver.BaseRequestHandler):
    """サーバに届いたリクエストを処理するクラス."""

    def handle(self) -> None:
        """
        クライアントからのリクエストを処理する関数.

        クライアントからのリクエストを受信する毎に実行される
        """
        # ヘッダ受信
        _header = self.request.recv(HEADER_SIZE)

        # ヘッダからデータサイズを取得 (受信データはbytesなのでintとして扱うためにunpackが必要)
        data_size = unpack('!I', _header)[0]
        print(data_size)

        # データ本体の受信
        _data = self.request.recv(data_size)
        print(_data)

        # 今回はサンプルなので受信したデータをそのまま返す
        # ヘッダ送信
        self.request.sendall(_header)
        # データ送信
        self.request.sendall(_data)


def start_server() -> None:
    """サーバ起動."""
    with socketserver.TCPServer((ADDR, PORT), MyTCPHandler) as s:
        # サーバとしてクライアントのリクエストを待ち続ける
        s.serve_forever()


if __name__ == '__main__':
    start_server()
