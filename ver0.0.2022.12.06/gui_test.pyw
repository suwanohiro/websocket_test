import PySimpleGUI as sg

# 画面構成
layout = [
    [sg.Text("テスト")],
    [sg.Text("状態："), sg.Text("　", key="status", background_color=("red"))],
    [sg.Button("test"), sg.Text("横に並べたい時はこう書く")],
    [sg.Button("test2", key="asd")]
]

# ウィンドウの生成
window = sg.Window("フォームタイトル", layout)

flg = False

# イベントループ
while True:
    event, values = window.read()

    if event == sg.WIN_CLOSED:
        # ウィンドウが閉じられた時に処理を抜ける
        break
    elif event == "asd":
        print("keyがasdのボタンが押されました。")
        color = "light green"
        if flg:
            color = "red"
        window["status"].update(background_color=(color))
        flg = not flg

window.close()
