import PySimpleGUI as sg


class Server:
    import asyncio
    import websockets

    PortNumber = 0
    ReceptionData = []  # 受信データ

    # この関数に通信しているときに行う処理を書く。
    # クライアントが接続している間は下の関数が常に回っている
    def BootUp(Port):
        try:
            if Port < 1 or Port > 65535:
                raise
            Server.PortNumber = int(Port)
            Server.asyncio.run(Server.main())
            print(f"{Port}番ポートでサーバーを起動しました。")
        except:
            print("サーバーの起動に失敗しました。")

    async def main():
        print("サーバー起動中")
        async with Server.websockets.serve(Server.handler, "0.0.0.0", int(Server.PortNumber)):
            await Server.asyncio.Future()  # run forever

    async def handler(websocket):
        # クライアントからのメッセージを取り出してそのまま送り返す（Echo）

        # print(websocket)

        async for message in websocket:
            print(message)
            await websocket.send(f"あなたが送信した文字は「{message}」です")


Server.BootUp(15140)
